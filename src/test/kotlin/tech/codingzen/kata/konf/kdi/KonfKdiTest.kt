package tech.codingzen.kata.konf.kdi

import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import tech.codingzen.kata.konfig.KataKonf
import tech.codingzen.kata.konfig.LensRootPair
import tech.codingzen.kata.konfig.env.EnvVarSource
import tech.codingzen.kata.konfig.retriever.KataKonfRetrieversLens
import tech.codingzen.kata.konfig.retriever.get
import tech.codingzen.kata.konfig.system_properties.SystemPropertiesLens
import tech.codingzen.kata.konfig.system_properties.SystemPropertiesSource
import tech.codingzen.kata.konfig.system_properties.sysprop
import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.res
import tech.codingzen.kdi.data_structure.*
import tech.codingzen.kdi.dsl.*
import tech.codingzen.kdi.logging.Logger
import tech.codingzen.kdi.spec.MultipleSpec

class KonfKdiTest {
  @Test
  fun test() = runBlocking {
    Kdi.appSpec()
      .printLogger(Logger.Level.TRACE)
      .scopeExtensions {
        KonfScope.scopeId.postModule {
          konf(::MyKonf)
        }
      }
      .spec(
        MultipleSpec(
          Kdi.moduleSpec("base") {
            tracked<LensRootPair>().instance(LensRootPair(KataKonfRetrieversLens, EnvVarSource(mapOf("a" to "12"))))
            tracked<LensRootPair>().instance(LensRootPair(SystemPropertiesLens, SystemPropertiesSource(mapOf("b" to "foo"))))
          },
          KonfScope))
      .execute { dsl ->
        val myKonf = dsl.get<MyKonf>()
        assertEquals(12, myKonf.a)
        assertEquals("foo", myKonf.b)
      }
  }

  class MyKonf(konf: KataKonf) {
    val a: Int by konf["a"]
    val b: String by konf.sysprop("b")
  }
}