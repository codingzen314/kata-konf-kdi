package tech.codingzen.kata.konf.kdi

import tech.codingzen.kata.konfig.KataKonfDsl
import tech.codingzen.kata.konfig.LensRootPair
import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.res
import tech.codingzen.kdi.data_structure.KdiModule
import tech.codingzen.kdi.data_structure.KdiModulesFactory
import tech.codingzen.kdi.dsl.module
import tech.codingzen.kdi.dsl.modules

class KonfKdiBootstrapper(private val pairs: Set<LensRootPair>) : KdiModulesFactory {
  override suspend fun create(): List<KdiModule> {
    val konf = KataKonfDsl { pairs.forEach { +it } }
    return modules {
      +module {
        instance(konf)
      }
    }
  }
}