package tech.codingzen.kata.konf.kdi

import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.spec.BaseSpec

object KonfScope : BaseSpec.Delegate {
  const val scopeId = "konf"
  override val scopeSpec: BaseSpec =
    Kdi.scopeSpec(scopeId)
      .bootstrapper { KonfKdiBootstrapper(getAll()) }
      .build()
}