package tech.codingzen.kata.konf.kdi

import tech.codingzen.kata.konfig.KataKonf
import tech.codingzen.kdi.dsl.module.ModuleDsl

inline fun <reified C> ModuleDsl.konf(crossinline creator: (KataKonf) -> C) {
  single { creator(get()) }
}