## Purpose
This library is an integration between kata-konf and kdi that allows the `KataKonf` instance to be created and managed within
Kdi.  In order to build up a `KataKonf` instance, add as many tracked `LensRootPair` components before `KonfScope` in the dependency 
injection order

There is a convenience method, `konf`, for adding typical KataKonf based configuration classes
```kotlin
module {
  konf(::MyKonf)
}

class MyKonf(konf: KataKonf) {
  val a: Int by konf["a"]
  val b: String by konf.sysprop("b")
}
```